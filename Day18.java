//Write your code here
/*
    Create a class Calculator which consists of a single method power(int,int). This method takes two       
    integers, n and p, as parameters and finds np. If either n or p is negative, then the method must throw 
    an exception which says "n and p should be non-negative".
*/

import java.util.*;
import java.io.*;


class Calculator {
    public int power(int base, int exponent) throws Exception {
        if (base < 0 || exponent < 0) {
            throw new Exception("n and p should be non-negative");
        }
        
        return (int) Math.pow(base, exponent);
    }
}

class Solution{

    public static void main(String []argh)
    {
        Scanner in = new Scanner(System.in);
        int T=in.nextInt();
        while(T-->0)
        {
            int n = in.nextInt();
            int p = in.nextInt();
            Calculator myCalculator = new Calculator();
            try
            {
                int ans=myCalculator.power(n,p);
                System.out.println(ans);
                
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }

    }
}
