import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here.*/
        Scanner read = new Scanner(System.in);
        int numCases = Integer.parseInt(read.nextLine());
        while(read.hasNextLine()) {
            int number = Integer.parseInt(read.nextLine());
            String binary = Integer.toBinaryString(number);
            System.out.println(binary);
        }
    }
}