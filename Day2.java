public class DataTypesPractice {

    public static void main(String[] args) {
      // Write your print statements here! You should have at least 8 of them.
      // Primitives: short, int, long, float, double, boolean, char
      
      /*
        5.35
        'a'
        false
        100
        "I am a code monkey"
        true
        17.3
        'c'
        "derp"
      */
      System.out.println("Primitive : Double");
      System.out.println("Primitive : Character");
      System.out.println("Primitive : Boolean");
      System.out.println("Primitive : Integer");
      System.out.println("Referenced : String");
      System.out.println("Primitive : Boolean");
      System.out.println("Primitive : Double");
      System.out.println("Primitive : Character");
      System.out.println("Referenced : String");
    }
}
