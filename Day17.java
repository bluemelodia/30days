import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner scanner = new Scanner(System.in);
        int size = Integer.parseInt(scanner.nextLine());
        String[] arr = scanner.nextLine().split(" ");
        int[] array = new int[size];
        for (int i = 0; i < size; i++) {
            array[i] = Integer.parseInt(arr[i]);
        }
        Arrays.sort(array);
        // 2 3 4 5 -> 2 3 3 4 4 5
        // find the mininum absolute difference
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < array.length-1; i++) {
            int abs = Math.abs(array[i+1] - array[i]);
            if (abs < min) {
                min = abs;
            }
        }
        
        String output = "";
        for (int i = 0; i < array.length-1; i++) {
            int abs = Math.abs(array[i+1] - array[i]);
            if (abs == min) {
                output += array[i] + " " + array[i+1] + " ";  
            }
        }
        System.out.println(output);
    }
    
    public static ArrayList<Integer> insertionSort(ArrayList<Integer> arr) {
        for (int i = 1; i < arr.size(); i++) {
            int position = i;
            for (int j = i-1; j >= 0; j--) {
                //System.out.println("compare: " + arr.get(position) + " and " + arr.get(j));
                if (arr.get(position) < arr.get(j)) {
                    int swap = arr.get(position);
                    arr.remove(position);
                    arr.add(j, swap);
                    position = j;
                }
            }
            //System.out.println(arr);
        }
        return arr;
    }
}