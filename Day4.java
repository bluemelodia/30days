    import java.io.*;
    import java.util.*;
    import java.text.*;
    import java.math.*;
    import java.util.regex.*;

    public class Solution {

        public static void main(String[] args) {
            /*
                Given an integer N as input, check the following:

                If N is odd, print "Weird".
                If N is even and, in between the range of 2 and 5(inclusive), print "Not Weird".
                If N is even and, in between the range of 6 and 20(inclusive), print "Weird".
                If N is even and N>20, print "Not Weird".
            */
            Scanner sc=new Scanner(System.in);
            int n=sc.nextInt();            
            String ans="";
            if(n%2==1)ans = "Weird";
            else
            {
               //Complete the code
               if ((n >= 2 && n <= 4) || n > 20) ans = "Not Weird";
               else ans = "Weird";
            }
            System.out.println(ans);
            
        }
    }
