import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.util.Scanner;

/*
	In this problem you will test your knowledge of loops. Given three integers a, b, and N, output the following series:
	a+20b,a+20b+21b,......,a+20b+21b+...+2N−1b
*/

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner inputReader = new Scanner(System.in);
        int numCases = Integer.parseInt(inputReader.nextLine());
        for (int i = 0; i < numCases; i++) {
            String[] testCase = inputReader.nextLine().split(" "); 
            
            String answer = "";
            int a = Integer.parseInt(testCase[0]);
            int b = Integer.parseInt(testCase[1]);
            int n = Integer.parseInt(testCase[2]);

            /*
                In the first case: a=5, b=3 ,N=5
                1st term =  5+(2^0×3)=8
                2nd term =  5+(2^0×3)+(2^1×3)=14
                3rd term =  5+(2^0×3)+(2^1×3)+(2^2×3)=26
                4th term =  5+(2^0×3)+(2^1×3)+(2^2×3)+(2^3×3)=50
                5th term =  5+(2^0×3)+(2^1×3)+(2^2×3)+(2^3×3)+(2^4×3)=98
            */
            double previous = a + Math.pow(2, 0)*b;
            answer += (int)previous;
            for (int k = 1; k < n; k++) {
                double sum = previous + Math.pow(2, k)*b;
                answer += " " + (int)sum;
                previous = sum;
            }
            System.out.println(answer);
        }
    }
}