//Complete this code or write your own from scratch
import java.util.*;
import java.io.*;

class Solution{
   public static void main(String []args)
   {
      HashMap map = new HashMap<String, Integer>();
      Scanner in = new Scanner(System.in);
      int N=in.nextInt();
      in.nextLine();
      for(int i=0;i<N;i++)
      {
         String name=in.nextLine();
         int phone=in.nextInt();
         map.put(name, phone);
         in.nextLine();
      }
      while(in.hasNext())
      {
         String s=in.nextLine();
         if (map.get(s) == null) System.out.println("Not found");
         else {
            System.out.println(s + "=" + map.get(s));
         }
      }
   }
}
