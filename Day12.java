import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    /*
        6x6 array (16 configs), find the max hourglass value
        a b c
          d
        e f g
    */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int arr[][] = new int[6][6];
        for(int i=0; i < 6; i++){
            for(int j=0; j < 6; j++){
                arr[i][j] = in.nextInt();
            }
        }
        int x = 0;
        int y = 0;
        int maxSum = -9*7;
        while (x <= 3 && y <= 3) {
            /*System.out.println("[" + x + "][" + y + "]" + arr[x][y]);
            System.out.println("[" + x + "][" + (y+1) + "]" + arr[x][y+1]);
            System.out.println("[" + x + "][" + (y+2) + "]" + arr[x][y+2]);
            System.out.println("[" + (x+1) + "][" + (y+1) + "]" + arr[x+1][y+1]);
            System.out.println("[" + (x+2) + "][" + y + "]" + arr[x+2][y]);
            System.out.println("[" + (x+2) + "][" + (y+1) + "]" + arr[x+2][y+1]);
            System.out.println("[" + (x+2) + "][" + (y+2) + "]" + arr[x+2][y+2]);*/
            int curSum = arr[x][y] + arr[x][y+1] + arr[x][y+2] + arr[x+1][y+1] + arr[x+2][y] + arr[x+2][y+1] + arr[x+2][y+2];
            //System.out.println("Sum: " + curSum);
            if (curSum > maxSum) maxSum = curSum;
            if (y == 3) { // go to the next row
                x++;
                y = 0;
            } else {
                y++;
            }
        }
        System.out.println(maxSum);
    }
}
