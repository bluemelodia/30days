import java.io.*;
import java.util.*;
public class Solution {
    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        //Take Input
        String[] vars = sc.nextLine().split(" ");
        int a = Integer.parseInt(vars[0]);
        int b = Integer.parseInt(vars[1]);
        int gcd=find_gcd(a,b);
        System.out.println(gcd);
    }
   static  int find_gcd(int a,int b){
          //Write the base condition
          //System.out.println("old: " + a + " " + b);
          if (b == 0) return a;
          //x' = MAX(x,y) - MIN(x,y) and y' = MIN(x,y)
          int new_b = Math.min(a, b);
          int new_a = Math.max(a, b) - Math.min(a, b);
          //System.out.println("new: " + new_a + " " + new_b);
          return find_gcd(new_b,new_a%new_b);
      }
}
 
