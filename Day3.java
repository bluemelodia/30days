import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Arithmetic {

    public static void main(String[] args) {
    
      Scanner sc = new Scanner(System.in);
      double M = sc.nextDouble(); // original meal price
      int T = sc.nextInt(); // tip percentage
      int X = sc.nextInt(); // tax percentage
        
      /*
      Three numbers, (M, T, and X), each on seperate lines:
      M will be a double representing the original price of the meal.
      T will be a integer representing the precentage that the customer wants to tip based off 
          of the original price of the meal.
      X will be an integer representing the tax percentage that the customer has to pay based off 
          of the original price of the meal. 
      */
      
      // tip = (meal price * tip)/100
      // tax = (meal price * tax)/100
        
      // Enter your code here!
      // Run some computations....
      double tip = (M*T)/100;
      double tax = (M*X)/100;
      int total = (int) Math.round(M + tip + tax);
      System.out.println("The final price of the meal is $" + total + ".");
      // int total = (int) Math.round(/*numberToRoundHere*/);
      
      // ...then print!
      
    }
}
