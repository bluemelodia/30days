import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


class Difference {
  	private int[] elements;
  	public int maximumDifference;

	// Add your code here
    public Difference(int[] array) {
        this.elements = array;
        this.maximumDifference = 0;
    }

    public void computeDifference() {
        for(int i = 0; i < this.elements.length; i++) {
            for(int j = 0; j < this.elements.length; j++) {
                if (i == j) continue;
                if (Math.abs(elements[i] - elements[j]) > this.maximumDifference) {
                    this.maximumDifference = Math.abs(elements[i] - elements[j]);
                }
            }
        }
    }
} // End of Difference class

public class Solution {

            public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int N = sc.nextInt();
                int[] a = new int[N];
                for (int i = 0; i < N; i++) {
                    a[i] = sc.nextInt();
                }

                Difference D = new Difference(a);

                D.computeDifference();

              	System.out.print(D.maximumDifference);
            }
        }